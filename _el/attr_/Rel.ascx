﻿<%@ Control Language="C#" AutoEventWireup="true" %>

developer.mozilla.org/en-US/docs/Web/HTML/Attributes/rel

The rel attribute defines the relationship between a linked resource and the current document. Valid on

	, and
	 the supported values depend on the element on which the attribute is found.

alternate
If hreflang is given alongside alternate, and the value of hreflang is different from the current document's language, it indicates that the referenced document is a translation.

If type is given alongside alternate, it indicates that the referenced document is an alternative format (such as a PDF).

The hreflang and type attributes may both be given alongside alternate.

...
